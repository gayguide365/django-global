from conf import GLOBAL_CSS, GLOBAL_JS, GLOBAL_CSS_TAGS, GLOBAL_JS_TAGS

from helpers import insert_html

class GlobalCssMiddleware:
    def process_response(self, request, response):
        css_tags = ['<link rel="stylesheet" type="text/css" href="%s">'%tag for tag in GLOBAL_CSS]
        return insert_html(response, GLOBAL_CSS_TAGS, css_tags)

class GlobalJsMiddleware:
    def process_response(self, request, response):
        js_tags = ['<script type="text/javascript" src="%s"></script>'%tag for tag in GLOBAL_JS]
        return insert_html(response, GLOBAL_JS_TAGS, js_tags)