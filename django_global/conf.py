from django.conf import settings

GLOBAL_CSS_TAGS = getattr(settings, "GLOBAL_CSS_TAGS", ("</head>", "<head>", "<body>", ))
GLOBAL_CSS = getattr(settings, "GLOBAL_CSS", [])
GLOBAL_JS_TAGS = getattr(settings, "GLOBAL_JS_TAGS", ("</body>", "</head>", ))
GLOBAL_JS = getattr(settings, "GLOBAL_JS", [])