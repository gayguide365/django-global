def insert(s, p, i):
    return s[:p] + i + s[p:]

def insert_html(response, find_tags, insert_tags):
    for tag in find_tags:
        if response.content.find(tag) != -1:
            if not "/" in tag:
                pos = response.content.find(tag) + len(tag)
            else:
                pos = response.content.find(tag)
            for insert_tag in insert_tags:
                response.content = insert(response.content, pos, insert_tag)
            return response
    return response